// class général
class stats {
  constructor(name, hp, atk, atkspe, nomatk, vit){
    this.name = name;
    this.hp = hp;
    this.atk = atk;
    this.atkspe = atkspe;
    this.nomatk = nomatk;
    this.vit = vit;
  }
}
// déclaration des variables
let persoGauche;
let persoDroite;
let executed = false;
let executed2 = false;
let vitesse = 2000;
let vitesseinfo = 2;
let feed = "";
let prefeed = 
"FIGHT: Début du duel" +
"\n" +
"\nSPEED: Temps entre chaque attaque en secondes, par défaut: 2sec (Modifiable entre 0 et ...)" +
"\n" +
"\nRESET: Nouveau duel. Attention, un reset pendant un combat nécessite de resélectionner ses personnages dans la liste déroulante" +
"\n" +
"\nConsulter le Patch Notes pour plus d'info sur les MAJs";
document.getElementById("feed").style.display = "none";
document.getElementById("prefeed").value = prefeed;

// patchnotes
let modal = document.getElementById("patch-popup");
let btn = document.getElementById("patchNotes");
let span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
  modal.style.display = "block";
};
span.onclick = function() {
  modal.style.display = "none";
};
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  };
};
// fonction inject réalisé avant le début du combat
function inject(){
  document.getElementById("prefeed").style.display = "none";
  document.getElementById("feed").style.display = "block";
  document.getElementById("btnfight").style.display = "none";
  document.getElementById("scorepv1").style.display = "block";
  document.getElementById("scorepv2").style.display = "block";
  document.getElementById("scorepv1").innerHTML = "HP Full";
  document.getElementById("scorepv2").innerHTML = "HP Full";
  document.getElementById("titre").style.display = "none";
  document.getElementById("pub").style.display = "none";
  document.getElementById("hero1").style.display = "none";
  document.getElementById("hero2").style.display = "none";
  document.getElementById("stats1").style.display = "none";
  document.getElementById("stats2").style.display = "none";
  document.getElementById("container").style.margin = "0 10% 0 10%";


  feed = ""
  feed = "Vitesse du jeu : " + vitesseinfo + " sec ";
  document.getElementById("feed").value = feed;
  // mode custom perso 1 et 2
  if ((document.getElementById("hero1").value) = "Custom"){
    persoGauche.name = document.getElementById("nameperso1").value;
    persoGauche.hp = parseFloat(document.getElementById("hpperso1").value);
    persoGauche.atk = parseFloat(document.getElementById("atkperso1").value);
    persoGauche.atkspe = parseFloat(document.getElementById("atkspeperso1").value);
    persoGauche.nomatk = document.getElementById("nameatkperso1").value;
    persoGauche.vit = parseFloat(document.getElementById("vitperso1").value);
    console.log(persoGauche);
  }
  if ((document.getElementById("hero2").value) = "Custom"){
    persoDroite.name = document.getElementById("nameperso2").value;
    persoDroite.hp = parseFloat(document.getElementById("hpperso2").value);
    persoDroite.atk = parseFloat(document.getElementById("atkperso2").value);
    persoDroite.atkspe = parseFloat(document.getElementById("atkspeperso2").value);
    persoDroite.nomatk = document.getElementById("nameatkperso2").value;
    persoDroite.vit = parseFloat(document.getElementById("vitperso2").value);
    console.log(persoDroite);
  }
  fight();
};
// qui joue en premier ?
let firstG = false;
function plusrapide(x,y){
  if (x > y){
    firstG = true;
  }
  else {
    firstG = false
  }
  // fonction random si la vitesse est égale
  if (persoGauche.vit == persoDroite.vit){
    let rand = Math.random();
    console.log(rand);
    if (rand > 0.5){
      firstG = false
      console.log(firstG);
    }
    else {
      firstG = true
      console.log(firstG);
    };
  };
};
// fonction executée au clique du bouton fight
function fight(){
  plusrapide(persoGauche.vit, persoDroite.vit);
  console.log("Qui joue en premier - true = JoueurGauche")
  console.log(firstG);
  // le plus rapide joue en premier
  if (firstG){
    i1 = 1;
    XtapeY(persoGauche, persoDroite);
  }
  else {
    i2 = 1;
    YtapeX(persoGauche, persoDroite);
  }
};
// fonction attaque de base
// x = persoGauche / y = persoDroite
let i1 = 0;
let i2 = 0;
console.log(i1);
console.log(i2);

// alternance des tours
function tourX(x,y){
  if (x.hp > 0){
    if (i1 <= 2 && !executed){
      console.log(executed + " atk normal tourX");
      executed = true;
      XtapeY(x,y);
      i1++;
      console.log(i1 + " atk normal tourX");
      };
    if (i1 >= 3 && !executed){
      console.log(executed + " fin atk spe tourX");
      executed = true;
      i1 = 0;
      XtapeYspe(x,y);
      console.log(i1 + " fin atk spe tourX");
    };
  };
};
function tourY(x,y){
  if (y.hp > 0){
    if (i2 <= 2 && !executed2){
      console.log(executed2 + " atk normal tourY");
      executed2 = true;
      YtapeX(x,y);
      i2++;
      console.log(i2 + " atk normal tourY");
    };
    if (i2 >= 3 && !executed2){
      console.log(executed2 + " fin atk spe tourY");
      executed2 = true;
      i2 = 0;
      YtapeXspe(x,y);
      console.log(i2 + " fin atk spe tourY");
    };
  };
};

// x = persoGauche / y = persoDroite
// les deux attaques de persoGauche
function XtapeY(x,y){
  document.getElementById("feed").value += "\n";
  // document.getElementById("feed").value += "\nTour de " + x.name + " :";
  y.hp = (y.hp - x.atk);
  document.getElementById("feed").value += "\n" + x.name + " attaque " + y.name;
  document.getElementById("feed").value += "\n" + "> -" + x.atk + " <";
  // document.getElementById("feed").value += "\n" + y.name + " Hp = " + y.hp;
  executed2 = false;
  gestionPv(x,y);
  setTimeout(tourY, vitesse, x, y);
};
function XtapeYspe(x,y){
  document.getElementById("feed").value += "\n";
  // document.getElementById("feed").value += "\nCombo de " + x.name + " :";
  y.hp = (y.hp - x.atkspe);
  document.getElementById("feed").value += "\n" + x.name + " utilise " + x.nomatk + " sur " + y.name;
  document.getElementById("feed").value += "\n" + ">> -" + x.atkspe + " <<";
  // document.getElementById("feed").value += "\n" + y.name + " Hp = " + y.hp;
  executed2 = false;
  gestionPv(x,y);
  setTimeout(tourY, vitesse, x, y);
}

// les deux attaques de persoDroite
function YtapeX(x,y){
  document.getElementById("feed").value += "\n";
  // document.getElementById("feed").value += "\nTour de " + y.name + " :";
  x.hp = (x.hp - y.atk);
  document.getElementById("feed").value += "\n" + y.name + " attaque " + x.name;
  document.getElementById("feed").value += "\n" + "> -" + y.atk + " <";
  // document.getElementById("feed").value += "\n" + x.name + " Hp = " + x.hp;
  executed = false;
  gestionPv(x,y);
  setTimeout(tourX, vitesse, x, y);
};
function YtapeXspe(x,y){
  document.getElementById("feed").value += "\n";
  // document.getElementById("feed").value += "\nCombo de " + y.name + " :";
  x.hp = (x.hp - y.atkspe);
  document.getElementById("feed").value += "\n" + y.name + " utilise " + y.nomatk + " sur " + x.name;
  document.getElementById("feed").value += "\n" + ">> -" + y.atkspe + " <<";
  // document.getElementById("feed").value += "\n" + y.name + " Hp = " + y.hp;
  executed = false;
  gestionPv(x,y);
  setTimeout(tourX, vitesse, x, y);
}

// gestion des pv dans les barres dynamiques
// x = persoGauche / y = persoDroite
function gestionPv(x,y){
  persoGauche = x;
  persoDroite = y;
  document.getElementById("scorepv1").innerHTML = persoGauche.hp + " HP";
  document.getElementById("scorepv2").innerHTML = persoDroite.hp + " HP";
  // si KO
  if (persoGauche.hp <= 0){
    document.getElementById("scorepv1").style.boxShadow = "2px 3px 20px 15px red";
    document.getElementById("feed").value += "\n";
    document.getElementById("feed").value += "\n" + persoDroite.name + " remporte le duel !";
  };
  if (persoDroite.hp <= 0){
    document.getElementById("scorepv2").style.boxShadow = "2px 3px 20px 15px red";
    document.getElementById("feed").value += "\n";
    document.getElementById("feed").value += "\n" + persoGauche.name + " remporte le duel !";
  };
};

// bouton reset 
function reset(){
  feed = "Vitesse du jeu : " + vitesseinfo + " sec ";
  document.getElementById("feed").value = feed;
  document.getElementById("feed").value += "\nPrêt pour un nouveau combat ?";
  document.getElementById("btnfight").style.display = "inline-block";
  document.getElementById("scorepv1").style.boxShadow = "2px 3px 20px 15px black";
  document.getElementById("scorepv2").style.boxShadow = "2px 3px 20px 15px black";
  document.getElementById("scorepv1").style.display = "none";
  document.getElementById("scorepv2").style.display = "none";
  document.getElementById("titre").style.display = "block";
  document.getElementById("pub").style.display = "block";
  document.getElementById("prefeed").style.display = "block";
  document.getElementById("feed").style.display = "none";
  document.getElementById("hero1").style.display = "block";
  document.getElementById("hero2").style.display = "block";
  document.getElementById("stats1").style.display = "block";
  document.getElementById("stats2").style.display = "block";
  document.getElementById("container").style.margin = "5% 10% 0 10%";
  executed = false;
  executed2 = false;
  i1 = 0;
  i2 = 0;
  persoGauche.hp = 0;
  persoDroite.hp = 0;
};

// choix de la vitesse des attaques
function speed(){
let speed = parseFloat(prompt("Entrer la vitesse entre les attaques en secondes"));
vitesse = speed * 1000;
vitesseinfo = speed;
feed = "\nNouvelle vitesse du jeu : " + vitesseinfo + " sec ";
document.getElementById("prefeed").value += "\n";
document.getElementById("feed").value += "\n";
document.getElementById("prefeed").value += feed;
document.getElementById("feed").value += feed;
};

// choix du skin1
persoGauche = new stats("Miyamoto Musashi", 5000, 750, 1000, "Katana", 99)
function switchSkin1(){
  let skin = document.getElementById("skin1");
  switch (document.getElementById("hero1").value){
    case "Samurai":
      skin.setAttribute("src", "./img/Nouveau dossier/sam.png");
      persoGauche = new stats("Miyamoto Musashi", 5000, 750, 1000, "Katana", 99);
      console.log(persoGauche);
      break;
    case "Wizard":
      skin.setAttribute("src", "./img/Nouveau dossier/sorcier.png");
      persoGauche = new stats("Gandalf", 5000, 500, 5000, "Boule de feu", 50);
      console.log(persoGauche);
      break;
    case "Godzila":
      skin.setAttribute("src", "./img/Nouveau dossier/godzila.png");
      persoGauche = new stats("Godzila", 50000, 2000, 5000, "Souffle de la mort ", 70);
      console.log(persoGauche);
      break;
    case "Jedi":
      skin.setAttribute("src", "./img/Nouveau dossier/jedi.png");
      persoGauche = new stats("Obi-Wan Kenobi", 4000, 2000, 6000, "la FORCE", 150);
      console.log(persoGauche);
      break;
    case "Military":
      skin.setAttribute("src", "./img/Nouveau dossier/militaire.png");
      persoGauche = new stats("Rambo", 1000, 150, 3000, "une Grenade", 40);
      console.log(persoGauche);
      break;
    // case "Custom":
    //   skin.setAttribute("src", "./img/Nouveau dossier/");
    //   break;
  }
  // charge les stats de la class dans les zones de texte
  document.getElementById("nameperso1").value = persoGauche.name;
  document.getElementById("hpperso1").value = persoGauche.hp;
  document.getElementById("atkperso1").value = persoGauche.atk;
  document.getElementById("atkspeperso1").value = persoGauche.atkspe;
  document.getElementById("nameatkperso1").value = persoGauche.nomatk;
  document.getElementById("vitperso1").value = persoGauche.vit;
  // console.log(persoGauche);
  
}
persoDroite = new stats("Gandalf", 5000, 500, 5000, "Boule de feu", 50);
function switchSkin2(){
  let skin = document.getElementById("skin2");
  switch (document.getElementById("hero2").value){
    case "Samurai":
      skin.setAttribute("src", "./img/Nouveau dossier/sam.png");
      persoDroite = new stats("Miyamoto Musashi", 5000, 750, 1000, "Katana", 99);
      console.log(persoDroite);
      break;
    case "Wizard":
      skin.setAttribute("src", "./img/Nouveau dossier/sorcier.png");
      persoDroite = new stats("Gandalf", 5000, 500, 5000, "Boule de feu", 50);
      console.log(persoDroite);
      break;
    case "Godzila":
      skin.setAttribute("src", "./img/Nouveau dossier/godzila.png");
      persoDroite = new stats("Godzila", 50000, 2000, 5000, "Souffle de la mort ", 70);
      console.log(persoDroite);
      break;
    case "Jedi":
      skin.setAttribute("src", "./img/Nouveau dossier/jedi.png");
      persoDroite = new stats("Obi-Wan Kenobi", 4000, 2000, 6000, "la FORCE", 150);
      console.log(persoDroite);
      break;
    case "Military":
      skin.setAttribute("src", "./img/Nouveau dossier/militaire.png");
      persoDroite = new stats("Rambo", 1000, 150, 3000, "une Grenade", 40);
      console.log(persoDroite);
      break;
    // case "Custom":
    //   skin.setAttribute("src", "./img/Nouveau dossier/");
    //   break;
  }
  // charge les stats de la class dans les zones de texte
  document.getElementById("nameperso2").value = persoDroite.name;
  document.getElementById("hpperso2").value = persoDroite.hp;
  document.getElementById("atkperso2").value = persoDroite.atk;
  document.getElementById("atkspeperso2").value = persoDroite.atkspe;
  document.getElementById("nameatkperso2").value = persoDroite.nomatk;
  document.getElementById("vitperso2").value = persoDroite.vit;
  // console.log(persoDroite);
}
